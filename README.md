# 获取亚马逊评论插件-使用教程

## 第一步下载插件到本地
![img.png](readfile/img.png)

## 修改IP
http://127.0.0.1:8091/report/amazon/saveReviews  
修改为你自己的服务器IP  
例如改为http://192.168.0.1:8091/report/amazon/saveReviews  

## 解压
![img_1.png](readfile/img_1.png)

## 打开谷歌浏览器
![img_2.png](readfile/img_2.png)

## 打开开发者模式
![img_3.png](readfile/img_3.png)

## 加载已解压的扩展程序
![img_4.png](readfile/img_4.png)

## 固定
![img_5.png](readfile/img_5.png)

## 打开需要抓取的页面
https://www.amazon.com/Pierre-Dumas-Womens-Patent-Syntheticsandals/product-reviews/B083P67YS3/ref=cm_cr_getr_d_paging_btm_next_8?ie=UTF8&pageNumber=8&reviewerType=all_reviews  

## 输入账号抓取
![img_6.png](readfile/img_6.png)